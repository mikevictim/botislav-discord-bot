from pony import orm

import config

# db instance
db = orm.Database()

# person model class
class Person(db.Entity):
    discord_name = orm.Required(str, unique=True)
    open_dota_id = orm.Optional(int)
    pubg_name = orm.Optional(str)

# add or update discord person
@orm.db_session
def add_or_update_person(discord_name: str, defaults: dict):
    if Person.exists(discord_name=discord_name):
        p = Person.get(discord_name=discord_name)
        for key, value in defaults.items():
            setattr(p,key,value)
    else:
        Person(discord_name=discord_name, **defaults)

# return person by his discord name
@orm.db_session
def get_person_by_name(discord_name: str):
    p = Person.get(discord_name=discord_name)
    return p

# init db
def init_db():
    db.bind(
        provider='postgres',
        user=config.DB_USER,
        password=config.DB_PASS,
        host=config.DB_HOST,
        database=config.DB_NAME
    )
    db.generate_mapping(create_tables=True)

