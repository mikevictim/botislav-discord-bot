
from commands.commands import get_bot

import config
from db.store import init_db

bot = get_bot()

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name}.')

bot.run(config.DISCORD_TOKEN)
