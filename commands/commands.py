import commands.emoji as emoji
import commands.utils as utl
import random
import discord
import safygiphy
import telegram
import twitch
import eightBallPhrases
from discord.ext import commands
from pubg_python import Shard
from pubg_python.exceptions import NotFoundError
from requests.exceptions import ReadTimeout, Timeout

import config
from db.store import add_or_update_person, get_person_by_name, init_db
from integrations.openDotaAPI.user import User
from integrations.pubgAPI.user import pubgUser

# twitch.tv/arthas
ARTHAS_ID = 27115707
ARTHAS = 'Arthas'

@commands.command(pass_context=True)
async def hello(ctx):
    """Greets user."""

    # send respond
    msg = '{} Здарова {}'.format(
        utl.respond_to(ctx),
        utl.get_emoji(ctx, emoji.ROFLAN_ZDAROVA)
    )
    await ctx.send(msg)


@commands.command(pass_context=True)
async def randomgif(ctx, tag: str):
    """Sends random gif to channel with provided tag."""

    # get random gif by tag from giphy
    g = safygiphy.Giphy(token=config.GIPHY_API_KEY)
    gif_url = g.random(tag=tag)['data']['images']['downsized_medium']['url']

    # send respond
    msg = '{} Лови случайную гифку {} \n{}'.format(
        utl.respond_to(ctx),
        gif_url,
        utl.get_emoji(ctx, emoji.MISHA_SWAG)
    )
    await ctx.send(msg)


@commands.command(pass_context=True)
async def podrubka(ctx):
    """Sends twitch.tv/arthas stream link if he streams."""

    # get twitch.tv/arthas stream
    twitchClient = twitch.TwitchClient(client_id=config.TWITCH_CLIENT_ID)
    stream = twitchClient.streams.get_stream_by_user(ARTHAS_ID)
    
    # send respond
    if stream is not None:
        msg = '{} Величайший стримит {} для {} работяг {} \n {}'.format(
            utl.respond_to(ctx),
            stream.get('game'),
            stream.get('viewers'),
            utl.get_emoji(ctx, emoji.ROFLAN_POMOIKA),
            stream.get('channel').get('url')
        )
    else:
        msg = '{} Величайший оффлайн {}'.format(
            utl.respond_to(ctx),
            utl.get_emoji(ctx, emoji.ROFLAN_POMOIKA)
        )
    await ctx.send(msg)


@commands.command(pass_context=True)
async def pubg(ctx):
    """Sends ststistics of last 15 matches in PUBG for specified username."""

    # get person
    person = get_person_by_name(
        utl.get_username(ctx) 
    )

    # check if pubg_name not empty
    if (not person) or (not person.pubg_name):
        msg = '{} Ты кто такой? Я тебя не знаю {}\nВоспользуйся `!addpubg`'.format(
            utl.respond_to(ctx),
            utl.get_emoji(ctx, emoji.ROFLAN_BULDIGA)
        )
        return await ctx.send(msg)

    # try to fetch pubg user by username
    try:  
        user = pubgUser(name=person.pubg_name)
    except (NotFoundError, AttributeError):
        # send respond if not found
        msg = '{} Не могу найти историю матчей {}'.format(
            utl.respond_to(ctx),
            utl.get_emoji(ctx, emoji.ROFLAN_POMINKI)
        )
        return await ctx.send(msg)
    
    # get table with last 15 matches as png image
    stats_msg, matches = user.recent_stats
  
    # construct embeded
    embed = discord.Embed(
        title=f"Cтата {person.pubg_name} за последние 14 матчей",
        description=stats_msg,
        color=0x00a0ea
    )
    embed.set_thumbnail(url=ctx.message.author.avatar_url)
    for name, value in matches:
        embed.add_field(name=name, value=value)
    
    # send respond
    msg = '{}'.format(
        utl.respond_to(ctx)
    )
    return await ctx.send(msg, embed=embed)


@commands.command(pass_context=True)
async def addpubg(ctx, name: str):
    """Adds your PUBG nickname allowing you to use !pubg."""

    # check if profile exists
    try:
        user = pubgUser(name=name)
    except (NotFoundError, AttributeError):
        # send respond if not found
        msg = '{} Не могу найти игрока с таким никнеймом {}'.format(
            utl.respond_to(ctx),
            utl.get_emoji(ctx, emoji.PEPE)
        )
        return await ctx.send(msg)
    
    # get person
    add_or_update_person(
        discord_name=utl.get_username(ctx),
        defaults={
            'pubg_name': name
        }
    )

    # send respond
    msg = '{} Привязал к тебе этот никнейм {}'.format(
        utl.respond_to(ctx),
        utl.get_emoji(ctx, emoji.ROFLAN_EBALO),
    )
    return await ctx.send(msg)


@commands.command(pass_context=True)
async def lastmatch(ctx):
    """Sends your last match from OpenDota."""
    
    # get person
    person = get_person_by_name(
        utl.get_username(ctx) 
    )

    # check if open_dota_id not empty
    if (not person) or (not person.open_dota_id):
        msg = '{} Ты кто такой? Я тебя не знаю {}\nВоспользуйся `!addopendota`'.format(
            utl.respond_to(ctx),
            utl.get_emoji(ctx, emoji.ROFLAN_BULDIGA)
        )
        return await ctx.send(msg)

    # try to get OpenDota user profile
    try:
        user = User(person.open_dota_id)
        lastmatch = user.lastmatch
        participant = lastmatch.get_participant(user.steamID)
        reaction = utl.get_emoji(ctx, emoji.FEELS_GOOD) if participant.win else utl.get_emoji(ctx, emoji.PEPE)
        embed = discord.Embed(
            title=f"{lastmatch.start_time} {lastmatch.game_mode} {reaction}",
            description="**{}** {} на\n**{}** со счетом {}\n\n{}**Урон по героям**: {}\n{}**Общая ценность**: {}\n\nПосмотреть на [OpenDota]({}) \n\n ".format(
                user.personaname,
                participant.win_or_lose_verb,
                participant.hero.localname,
                participant.score,
                utl.get_emoji(ctx, emoji.ATTACK),
                participant.hero_damage,
                utl.get_emoji(ctx, emoji.GOLD),
                participant.total_gold,
                lastmatch.url
            ),
            color=0x00a0ea
        )
        embed.set_thumbnail(url=participant.hero.image_vert_url)
    except ReadTimeout:
        # send respond if OpenDota not avaliable
        msg = '{} OpenDota лежит и не встает {}'.format(
            utl.respond_to(ctx),
            utl.get_emoji(ctx, emoji.ROFLAN_POMINKI)
        )
        return await ctx.send(msg)
    
    # send respond
    msg = '{}'.format(
        utl.respond_to(ctx)
    )
    return await ctx.send(msg, embed=embed)

@commands.command(pass_context=True)
async def addopendota(ctx, id: int):
    """Adds your opendota id allowing you to use !lastmatch."""
    
    # check if profile exists
    try:
        user = User(id)
    except KeyError:
        # send respond if profile with provided id not found
        msg = '{} Не могу найти профиль с таким id {}'.format(
            utl.respond_to(ctx),
            utl.get_emoji(ctx, emoji.PEPE)
        )
        return await ctx.send(msg)
    
    # add opendota id to person
    add_or_update_person(
        discord_name=utl.get_username(ctx),
        defaults={
            'open_dota_id': id
        }
    )

    # send respond
    msg = '{} привязал к тебе этот профиль {}\n{}'.format(
        utl.respond_to(ctx),
        utl.get_emoji(ctx, emoji.ROFLAN_EBALO),
        f'https://www.opendota.com/players/{id}'
    )
    return await ctx.send(msg)


@commands.command(pass_context=True)
@commands.cooldown(1, 360, commands.BucketType.channel)
async def summon(ctx):
    """Summons old mans to channel."""
    
    # get telegram bot
    telegram_bot = telegram.Bot(token=config.TELEGRAM_TOKEN)
   
    # send summon message
    text = '@iFesh @hox70n @originalgish @mrmerkone @JunkTapes {} вызывает олдов в mumblach'.format(
        utl.get_username(ctx)
    )
    telegram_bot.send_message(
        chat_id=config.TELEGRAM_CHAT_ID,
        text=text
    )

    # send respond
    msg = '{0} суммоню олдов {1}'.format(
        utl.respond_to(ctx),
        utl.get_emoji(ctx, emoji.ROFLAN_ZDAROVA)
    )
    return await ctx.send(msg)

@commands.command(pass_context=True,aliases=['8ball','8all','8'])
async def eight_ball(ctx,args: str):
    """Asks the Bot for his opinion."""
    
    if (len(args) == 0):
        await ctx.send("Что бы ты хотел меня спросить?")
    else:
        await ctx.send(random.choice(eightBallPhrases.rspnBall))

async def handle_error(ctx, exc):
    """Simple command error handler."""
    
    if isinstance(exc, commands.CommandOnCooldown):
        msg = '{} Команда на кулдауне еще {:.2f}с {}'.format(
            utl.respond_to(ctx),
            exc.retry_after,
            utl.get_emoji(ctx, emoji.ROFLAN_GORIT),
            )
        await ctx.send(msg)

    elif isinstance(exc, commands.MissingRequiredArgument):
        msg = '{} Недостаточно аргументов. Воспользуйся ```!help <command_name>```'.format(
                utl.respond_to(ctx)
            )
        await ctx.send(msg)
    
    else:
        print(exc)


def get_bot():
    """Setup commands and return bot."""

    # get bot
    bot = commands.Bot(command_prefix='!', description='Botislav')
    
    # setup commands
    bot.add_command(hello)
    bot.add_command(randomgif)
    bot.add_command(podrubka)
    bot.add_command(pubg)
    bot.add_command(addpubg)
    bot.add_command(lastmatch)
    bot.add_command(addopendota)
    bot.add_command(summon)
    bot.add_command(eight_ball)
    
    # setup listeners
    bot.add_listener(handle_error, "on_command_error")
    
    # init db
    init_db()
    
    return bot
