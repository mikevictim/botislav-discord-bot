from discord.utils import get
from discord.ext.commands import Context
from discord.utils import get


def get_username(ctx: Context):
    '''
    Returns discord username as str.
    '''
    return '{0.author}'.format(ctx)

def respond_to(ctx: Context):
    '''
    Returns discord user mention as str.
    '''
    return '{0.author.mention}'.format(ctx)

def get_emoji(ctx: Context, name: str):
    '''
    Returns str emoji <:emoji_name:emoji_id> from client by its lookup.
    '''
    return get(ctx.bot.emojis, name=name)

