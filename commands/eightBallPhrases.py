rspnBall = [
        'Очень маловероятно,что это так.',
        'На этот вопрос ответит - Александр Друзь!',
        'Скорее всего да,чем нет.',
        'Бьюсь об заклад,что это так!',
        'Ответ на этот вопрос - 42.',
        'Вы точно правильно сформулировали вопрос?',
        'Определенно да.',
        'Все зависит от точки зрения.',
        'Скорее всего нет,чем да.',
        'Я бы так не сказал.',
        'А что если вы спросите у себя?',
        'Ни в коем случае!',
        'Я уверен,что да.',
        'Я уверен,что нет.',
        'Я не уверен,что положительный ответ вас устроит.',
        'Я не уверен,что отрицательный ответ вас устроит.',
        'В интернетах об этом не пишут.',
        'Не имею ни малейшего понятия.',
        'Это вопрос времени.',
        'Бесспорно.',
        'В этом нет никаких сомнений.',
        'Можете быть уверены в этом.',
        'Мне кажется что - да.',
        'Вероятнее всего да.',
        'Знаки космоса говорят - да.',
        'Пока не известно, попробуйте снова.',
        'Лучше вам об этом не знать.',
        'Даже и не думайте об этом.',
        'Мои источники утверждают обратное.',
        'Весьма сомнительно.',
        'По моим данным - нет.'
    ]