import json
from datetime import datetime

import requests

from integrations.openDotaAPI.match import Match


class User(object):

    def __get_lastmatch(self):
        r = requests.get(
            "https://api.opendota.com/api/players/{}/matches?significant={}&limit=1".format(self.steamID, 0))
        self.__lastmatch = json.loads(r.text)[0]

    def __get_profile(self):
        r = requests.get(
            "https://api.opendota.com/api/players/{}".format(self.steamID), timeout=10)
        self.__profile = json.loads(r.text)["profile"]


    def __init__(self, steamID):
        self.steamID = steamID
        self.__get_profile()
        self.__get_lastmatch()

    @property
    def personaname(self):
        return self.__profile["personaname"]

    @property
    def lastmatch(self):
        return Match(self.__lastmatch["match_id"])
