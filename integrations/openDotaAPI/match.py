import json

import requests
from datetime import datetime, timedelta


GAME_MODES = [
    "Unknown",
    "All pick",
    "Captains mode",
    "Random draft",
    "Single draft",
    "All random",
    "Intro",
    "Diretide",
    "Reverse captains mode",
    "Greeviling",
    "Tutorial",
    "Mid only",
    "Least played",
    "Limited heroes",
    "Compendium matchmaking",
    "Custom",
    "Captains draft",
    "Balanced draft",
    "Ability draft",
    "Event",
    "All random death match",
    "1v1 mid",
    "All draft",
    "Turbo",
    "Mutation"
]

class Hero(object):
    """
    Represents hero object from OpenDota API.
    """

    def __init__(self, hero_id):
        r = requests.get("https://api.opendota.com/api/heroes")
        self.__data = json.loads(r.text)
        self.hero_id = hero_id
        for i in self.__data:
            if i["id"] == hero_id:
                self.__data = i

    @property
    def name(self):
        return self.__data["name"][14:]

    @property
    def localname(self):
        return self.__data["localized_name"]
        
    @property
    def roles(self):
        return self.__data["roles"]
    
    @property
    def image_sb_url(self):
        return 'https://api.opendota.com/apps/dota2/images/heroes/{}_sb.png'.format(self.name)

    @property
    def icon_url(self):
        return 'https://api.opendota.com/apps/dota2/images/heroes/{}_icon.png'.format(self.name)

    @property
    def image_vert_url(self):
        return 'https://api.opendota.com/apps/dota2/images/heroes/{}_vert.jpg'.format(self.name)

class MatchParticipant(object):
    """
    Represents match participant from Match object.
    """

    def __init__(self, data):
        self.__data = data

    @property
    def kills(self):
        return self.__data['kills']

    @property
    def deaths(self):
        return self.__data['deaths']

    @property
    def assists(self):
        return self.__data['assists']

    @property
    def kda(self):
        if self.__data['deaths'] == 0:
            return self.kills + self.assists
        return (self.kills + self.assists)/self.__data['deaths']

    @property
    def score(self):
        return "{}/{}/{}".format(
            self.kills,
            self.deaths,
            self.assists
        )

    @property
    def total_gold(self):
        if self.__data['total_gold'] < 1000:
            return "%d" % int(self.__data['total_gold'])
        return "{:0.2f}k".format(int(self.__data['total_gold'])/1000)

    @property
    def tower_damage(self):
        if self.__data['tower_damage'] < 1000:
            return "%d" % int(self.__data['tower_damage'])
        return "{:0.2f}k".format(int(self.__data['tower_damage'])/1000)

    @property
    def hero_damage(self):
        if self.__data['hero_damage'] < 1000:
            return "%d" % int(self.__data['hero_damage'])
        return "{:0.2f}k".format(int(self.__data['hero_damage'])/1000)

    @property
    def gpm(self):
        return self.__data['gold_per_min']

    @property
    def xpm(self):
        return self.__data['xp_per_min']

    @property
    def win(self):
        return self.__data['win']

    @property
    def hero(self):
        return Hero(self.__data['hero_id'])

    @property
    def win_or_lose_verb(self):
        
        if self.deaths == 0:
            return "отыграл без ошибок"

        if self.win:            
            if self.kda <= 1:
                return "старался слить, но не смог"
            if self.kda > 1 and self.kda  <= 1.5:
                return "был якорем"
            if self.kda > 1.5 and self.kda  <= 2:
                return "играл с бустером"
            if self.kda > 2 and self.kda  <= 3:
                return "жал кнопки вовремя"
            if self.kda > 3 and self.kda  <= 4:
                return "затащил"
            if self.kda > 4:
                return "залил соляры"
        else:
            if self.kda > 4:
                return "проиграл 1vs9"
            if self.kda > 3 and self.kda  <= 4:
                return "очень обидно проиграл"
            if self.kda > 2 and self.kda  <= 3:
                return "пытался победить"
            if self.kda > 1.5 and self.kda  <= 2:
                return "закинул"
            if self.kda > 1 and self.kda  <= 1.5:
                return "не понимал, что происходит"
            if self.kda <= 1:
                return "стоял АФК"


class Match(object):
    """
    Represents match object from OpenDota API.
    """

    def __init__(self, match_id):
        self.match_id = match_id
        r = requests.get("https://api.opendota.com/api/matches/{}".format(self.match_id))
        self.__data = json.loads(r.text)

    @property
    def url(self):
        return "https://www.opendota.com/matches/{}".format(self.match_id)
        
    @property
    def duration(self):
        seconds =self.__data["duration"]
        m = seconds//60
        s = seconds-(m*60)
        return "{:02d}:{:02d}".format(m,s)

    @property
    def game_mode(self):
        return GAME_MODES[self.__data["game_mode"]]

    @property
    def start_time(self):
        time = datetime.fromtimestamp(self.__data["start_time"]).strftime('%d-%b-%Y')
        return time

    def get_participant(self, account_id):
        for p in self.__data["players"]:
            if p["account_id"] == int(account_id):
                return MatchParticipant(p)

