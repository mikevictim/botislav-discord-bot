# -*- coding: utf-8 -*-
from datetime import datetime

import pandas as pd
from pubg_python import PUBG, Shard

import config

PUBG_MAPS = {
    'Savage': 'Sanhok',
    'Erangel': 'Erangel',
    'DihorOtok': 'Vikendi',
    'Desert': 'Miramar',
    'Range': 'Training'
}

class pubgUser(object):

    MATCHES_COUNT = 14 

    def __init__(self, name, shard=Shard.PC_RU):
        self.api = PUBG(config.PUBG_API_KEY, Shard(shard))
        self.player = self.api.players().filter(player_names=[name])[0]
    
    @property
    def recent_stats(self):
        data = []
        for match in self.player.matches[:self.MATCHES_COUNT]:
            match_data = self.api.matches().get(match.id)
            for roster in match_data.rosters:
                for participant in roster.participants:
                    if participant.name == self.player.name:
                        date = datetime.strptime(match_data.created_at, '%Y-%m-%dT%H:%M:%SZ')
                        data.append(
                            (
                            date.strftime('%b-%d %H:%M'),
                            PUBG_MAPS[match_data.map_name[:-5]],
                            int(participant.win_place),
                            int(participant.kills),                                   
                            int(participant.headshot_kills),                                   
                            int(participant.longest_kill),
                            f"[pubg.sh replay](https://pubg.sh/{self.player.name}/steam/{match.id})"
                            )
                        )
        labels = ['Date','Map','Rank','Kills','HeadSh','Longest','Replay']
        df = pd.DataFrame.from_records(data, columns=labels)
        dinner = '🍗' if df['Rank'].min() == 1 else df['Rank'].min()
        msg = " ⬆️ Лучшее место - {}\n 💀 Всего убийств - {}\n 💥 Из них в голову - {}\n 📏 Самое дальнее - {} м.".format(
            dinner,
            df['Kills'].sum(),
            df['HeadSh'].sum(),
            df['Longest'].max()
        )
        embed_matches = []
        for index, row in df.iterrows():
            embed_matches += [(f"{row['Date']} {row['Map']}", f"⬆️ - {'🍗' if row['Rank'] == 1 else row['Rank']} 💀 - {row['Kills']}\n{row['Replay']}"),]
        return (msg, embed_matches)

    def _get_last_matches(self):
        self.player.matches[:self.MATCHES_COUNT]
