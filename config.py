import os

try:
    import dev_config
    dev_config.setup_env()
except ImportError:
    pass

# discord bot api  
DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')

# pubg api
PUBG_API_KEY = os.getenv('PUBG_API_KEY')

# giphy api
GIPHY_API_KEY = os.getenv('GIPHY_API_KEY')

# twitch api
TWITCH_CLIENT_ID = os.getenv('TWITCH_CLIENT_ID')

# telegram
TELEGRAM_TOKEN = os.getenv('TELEGRAM_TOKEN')
TELEGRAM_CHAT_ID = os.getenv('TELEGRAM_CHAT_ID')

# database
DB_HOST = os.getenv('DB_HOST')
DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')