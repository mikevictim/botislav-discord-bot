# Botislav discord bot
[![pipeline status](https://gitlab.com/mrmerkone/botislav-discord-bot/badges/master/pipeline.svg)](https://gitlab.com/mrmerkone/botislav-discord-bot/commits/master)
## Commands
* `!addopendota <open_dota_id>` Adds your opendota id allowing you to use !lastmatch.
* `!lastmatch` Sends your last match from OpenDota.
* `!addpubg <pubg_nickname>` Adds your PUBG nickname allowing you to use !pubg.
* `!pubg` Sends statistics of last 15 matches in PUBG for specified user.
* `!podrubka` Sends twitch.tv/arthas stream link if he streams.
* `!randomgif <tag>` Sends random gif to channel with provided tag.
* `!summon` Summons old mans to channel by sending message to provided Telegram messanger group.
* `!hello` Greets user.
* `!8ball` Asks Bot about his opinion.
* `!help` Shows this message.

## Startup
1. Install dependencies:
    * `$ pip install -r requirements.txt`
2. Setup your tokens and api keys in `config.py`
3. Run bot:
    * `$ python bot.py`